;;; hiddenquote-gen.el --- Code to create hidden quote puzzles -*- lexical-binding: t -*-

;; Copyright (C) 2020-2023 by Mauro Aranda

;; Author: Mauro Aranda <maurooaranda@gmail.com>
;; Maintainer: Mauro Aranda <maurooaranda@gmail.com>
;; Created: Sun Dec 13 11:10:00 2020
;; Version: 0.1
;; Package-Version: 0.1
;; Package-Requires: ((emacs "25.1") (hiddenquote "0.1"))
;; URL: http://www.mauroaranda.com
;; Keywords: games

;; This file is NOT part of GNU Emacs.

;; hiddenquote-gen is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; hiddenquote-gen is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with hiddenquote-gen. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; This file contains utility functions and a command to create hidden
;; quote puzzles, and save them in an ipuz format.
;;
;; Usage:
;; 1. Select a quote you want to use for the puzzle (this work is manual).
;; For example, use my quotes package to load all quotes from a file with
;; `quotes-load-quotes-from-file' and then select one of the quotes.
;; 2. Use the function `hiddenquote-gen-generate-regexps' to get a list of
;; regexps. If you want to, you can choose the positions for the
;; arrows yourself.
;; 3. Use the returned ordered list of regexps to select words
;; from a dictionary (this work is manual).  Then syllabify each word,
;; saving the definitions, the word itself and the syllables.
;; 4. Use the command `hiddenquote-gen-create-ipuz-file' to create the
;; ipuz file for the format.  When you are done editing, save the file by
;; using the button "Create ipuz file".  The file will be saved under
;; a directory called "puzzles", inside `hiddenquote-directory', with the
;; following name: ID.ipuz
;; 5. To test everything went fine, type C-u M-x hiddenquote, and select
;; the hidden-quote source.  Of course, make sure the `hiddenquote-sources'
;; contains an entry to find and retrieve this puzzle.  Enter the ID of the
;; created puzzle, and you should see the grid to play the puzzle you've just
;; created.
;;
;; Current limitations:
;; - Characters in the quote should have no modifier.  If the quote has "á",
;; write it as "a".  When looking for the grid words, it is not necessary
;; that the word matches the modifier, so it's enough if the word contains "a".
;; - It's better if the length of the transformed quote (a quote with no
;; punctuaction and whitespace) is an even number, because it's easier if
;; there are no further restrictions for the words that form the quote
;; in the grid.
;;
;; TODO:
;; - Allow a single arrow, or three.  Of course three makes it harder to find
;; words.  A single arrow helps to create hiddenword puzzles.
;; - Allow creation of the puzzle with a Puzzle UI, rather than a Customize UI.

;;; Code:

;; Requirements.
(require 'hiddenquote)
(require 'wid-edit)
(require 'eieio)
(require 'eieio-custom)
(require 'cl-lib)

;; eieio-custom.el
(defvar eieio-wo)
(defvar eieio-co)

;; User options.
(defgroup hiddenquote-gen nil
  "Options for the creation of hidden quote puzzles."
  :group 'hiddenquote)

(defcustom hiddenquote-gen-regexp 'posix
  "Which type the built regexps for looking up words should be.

Choices are: posix, elisp or buscapalabra.

If using the dictionary package that comes with Emacs, use posix.

The buscapalabra value is useful for creating puzzles in Spanish."
  :type '(choice (const :tag "Use Posix regexps" posix)
		 (const :tag "Use Elisp regexps" elisp)
                 (const :tag "dCode regexps that work" dcode)
                 (const :tag "The buscapalabra.com style" buscapalabra))
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-ipuz-version "http://ipuz.org/v2"
  "A string, the ipuz version used when creating ipuz files."
  :type 'string
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-ipuz-kind
  ["http://mauroaranda.com/puzzles/hiddenquote#1"
   "http://ipuz.org/crossword#1"]
  "A vector of URLs, suitable as the kind field for the ipuz file."
  :type '(vector (repeat :inline t (string :tag "PuzzleType")))
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-ipuz-publisher "Some Name"
  "Default publisher name when creating an ipuz file."
  :type 'string
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-ipuz-base-url
  "http://yourdomain.com"
  "Base URL, to use in `hiddenquote-gen-build-url'."
  :type 'string
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-ipuz-origin "GNU Emacs"
  "Default string to use for the origin field in the ipuz file."
  :type 'string
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-author-name user-login-name
  "Default author name when creating an ipuz file."
  :type 'string
  :package-version '(hiddenquote-gen . "0.1"))

(defcustom hiddenquote-gen-editor-name user-login-name
  "Default editor name when creating an ipuz file."
  :type 'string
  :package-version '(hiddenquote-gen . "0.1"))

;; Utils.
(defun hiddenquote-gen-randomize-arrows ()
  "Return a cons-cell of two random integers, for use in grid arrows.

It doesn't return (4 . 5), because I've never seen this happen.  So,
if the car is 4, the cdr is guaranteed to be 6 or greater."
  (let ((first (1+ (random 4)))
        (second (+ 4 (1+ (random 4)))))
    ;; I've just never seen this happen.
    (when (and (= first 4) (= second 5))
      (setq second 6))
    (cons first second)))

;; Methods.
(cl-defmethod eieio-custom-object-apply-reset
  ((_self hiddenquote-hidden-quote-puzzle))
  "Insert buttons in the buffer for customizing SELF.

SELF is the object to save in an ipuz file.

Creates two buttons, one for creating the ipuz file and other one to cancel
the operation."
  (widget-create 'push-button
                 :action (lambda (&rest _)
                           (widget-apply eieio-wo :value-get)
                           (eieio-done-customizing eieio-co)
                           (bury-buffer))
                 "Create ipuz file")
  (widget-insert "   ")
  (widget-create 'push-button
                 :action (lambda (&rest _)
                           (bury-buffer))
                 "Cancel"))

(cl-defmethod hiddenquote-gen-build-copyright
  ((self hiddenquote-hidden-quote-puzzle))
  "Build a copyright string for SELF."
  (concat "Copyright (C) " (format-time-string "%Y" (current-time)) " "
          (oref self author)))

(cl-defmethod hiddenquote-gen-build-url
  ((self hiddenquote-hidden-quote-puzzle))
  "Build an unique url for SELF."
  (concat hiddenquote-gen-ipuz-base-url (number-to-string (oref self id))))

(cl-defmethod hiddenquote-gen-build-title
  ((self hiddenquote-hidden-quote-puzzle))
  "Build a title for SELF."
  (format "Hidden Puzzle Nº%d" (oref self id)))

(cl-defmethod eieio-done-customizing ((self hiddenquote-hidden-quote-puzzle))
  "Create ipuz file from SELF when done customizing."
  (with-temp-buffer
    (insert (hiddenquote-puzzle-to-ipuz self))
    (let ((file (hiddenquote-expand-puzzle-file-name
                 (format "%s.ipuz" (oref self id))
                 "puzzles")))
      (hiddenquote-ensure-file-exists file)
      (write-file file))))

;; Functions.
(defun hiddenquote-gen-strip-quote (q)
  "Return Q, stripped from whitespace and punctuation characters."
  (replace-regexp-in-string "\\([[:blank:]]\\|[[:punct:]]\\)" "" q))

(defun hiddenquote-gen-split-quote-in-half (q)
  "Return a cons-cell of two strings, the result of splitting Q in halves.

If the length of Q is odd, then the car of the result is 1 character longer
than the cdr."
  (let* ((len (length q))
         (hlen (if (= (% len 2) 0)
                   (/ len 2)
                 (1+ (/ len 2)))))
    (cons (substring q 0 hlen) (substring q hlen))))

(defun hiddenquote-gen-generate-regexp (ch1 ch2 pos1 pos2)
  "Generate a regexp suitable for looking for words in a dictionary.

CH1 and CH2 should be 1-length strings or characters.
POS1 and POS2 should be integers.

The resulting regex matches any word that has CH1 at POS1 and CH2 at POS2."
  (or (stringp ch1) (setq ch1 (char-to-string ch1)))
  (or (stringp ch2) (setq ch2 (char-to-string ch2)))
  (cond ((eq hiddenquote-gen-regexp 'posix)
         (concat "^" (unless (= pos1 1)
                       (concat "[[:alpha:]]{"
                               (number-to-string (1- pos1))
                               "}"))
                 ch1 "[[:alpha:]]{" (number-to-string (1- (- pos2 pos1))) "}"
                 ch2 "[[:alpha:]]{0," (number-to-string (1+ (random 2))) "}"))
        ((eq hiddenquote-gen-regexp 'dcode)
         (concat "^" (unless (= pos1 1)
                       (concat ".{" (number-to-string (1- pos1)) "}"))
                 ch1 ".{" (number-to-string (1- (- pos2 pos1))) "}"
                 ch2 ".{0," (number-to-string (1+ (random 2))) "}"))
        ((eq hiddenquote-gen-regexp 'elisp)
         (concat "\\<" (unless (= pos1 1)
                         (concat "[[:alpha:]]\\{"
                                 (number-to-string (1- pos1))
                                 "\\}"))
                 ch1  "[[:alpha:]]\\{"
                 (number-to-string (1- (- pos2 pos1)))
                 "\\}"
                 ch2 "[[:alpha:]]\\{0,"
                 (number-to-string (1+ (random 2)))
                 "\\}"))
        ((eq hiddenquote-gen-regexp 'buscapalabra)
         (concat (make-string (1- pos1) ?.) ch1
                 (make-string (1- (- pos2 pos1)) ?.) ch2
                 (make-string (1+ (random 4)) ?.)))
        (t
         (error "Bad value for hiddenquote-gen-regexp: %s"
                hiddenquote-gen-regexp))))

(defun hiddenquote-gen-generate-regexps (q &optional arrows rtype)
  "Return a list of regexps to use to find words that form the quote Q.

ARROWS should be a cons-cell of two integers, or nil if you want it random.
Each integer represents a string position and the characters in those
positions will form the quote Q.

If RTYPE is non-nil, use that as the value of `hiddenquote-gen-regexp'."
  (let* ((split (hiddenquote-gen-split-quote-in-half
                 (hiddenquote-gen-strip-quote (downcase q))))
         (arrows (or arrows (hiddenquote-gen-randomize-arrows)))
         (spl1 (car split))
         (spl2 (cdr split))
         (arr1 (car arrows))
         (arr2 (cdr arrows))
         (hiddenquote-gen-regexp (or rtype hiddenquote-gen-regexp))
         (len2 (length spl2))
         lst)
    (dotimes (i (length (car split)))
      (push (hiddenquote-gen-generate-regexp (aref spl1 i)
                                             (if (< len2 (1+ i))
                                                 ""
                                               (aref spl2 i))
                                             arr1 arr2)
            lst))
    (reverse lst)))

;; Commands.
;;;###autoload
(defun hiddenquote-gen-create-ipuz-file ()
  "Pop a customize-like buffer to create a new ipuz file."
  (interactive)
  (let ((puzzle (make-instance 'hiddenquote-hidden-quote-puzzle
                               :version hiddenquote-gen-ipuz-version
                               :kind hiddenquote-gen-ipuz-kind
                               :origin hiddenquote-gen-ipuz-origin
                               :publisher hiddenquote-gen-ipuz-publisher
                               :author hiddenquote-gen-author-name
                               :editor hiddenquote-gen-editor-name)))
    (oset puzzle copyright (hiddenquote-gen-build-copyright puzzle))
    (oset puzzle url (hiddenquote-gen-build-url puzzle))
    (oset puzzle title (hiddenquote-gen-build-title puzzle))
    (eieio-customize-object puzzle)))

(provide 'hiddenquote-gen)
;;; hiddenquote-gen.el ends here
